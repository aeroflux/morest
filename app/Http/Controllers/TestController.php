<?php

namespace App\Http\Controllers;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getData() {
        $client = new \GuzzleHttp\Client();
        $req = $client->request('GET', 'https://developers.zomato.com/api/v2.1/categories', [
            'headers'=> [
                'user-key' => 'b552ba47602de196c371c7c918db70a4'
            ]
        ]);
        $res = $req->getBody()->getContents();
        print_r($res);
    }

    public function postData() {
        $client = new \GuzzleHttp\Client();
        $req = $client->request('POST', 'https://developers.zomato.com/api/v2.1/categories', [
            'headers'=> [
                'user-key' => 'b552ba47602de196c371c7c918db70a4'
            ]
        ]);
        $res = $req->getBody()->getContents();
        print_r($res);
    }

    //
}
